package com.task.tech.controllers;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.task.tech.entities.Disk;
import com.task.tech.entities.TakenItem;
import com.task.tech.entities.User;
import com.task.tech.services.DiskService;
import com.task.tech.services.TakenItemService;
import com.task.tech.services.UserService;

/**
 * Handles and retrieves disks' request
 */
@Controller
public class DiskController 
{
	@Autowired
	UserService userService;
	
	@Autowired
	DiskService diskService;
	
	@Autowired
	TakenItemService takenItemService;
	
	 /**
	  * Handles and retrieves specified disks and show it in a JSP page
	  *
	  * @return the name of the JSP page
	  */
	@RequestMapping(value="/user/{userId}/disks", method=RequestMethod.GET)
	public String getDisks(Model model, @PathVariable("userId") int userId, @RequestParam("type") String diskType)
	{
		User user = userService.getById(userId);
		List<Disk> disks;
		
		switch(diskType)
		{
			//This type of disks specifies render disks rented from user
			case "rentedFrom":
			{
				disks = diskService.getRentedFrom(userId);
				model.addAttribute("listType", "disks rented from " + user.getFullName());
			}
			break;
			//This type of disks specifies render disks returned to user
			case "returnedTo":
			{
				disks = diskService.getReturnedTo(userId);
				model.addAttribute("listType", "disks returned to " + user.getFullName());
			}
			break;
			//This type of disks specifies render disks rented by user
			case "rentedBy":
			{
				disks = diskService.getRentedBy(userId);
				model.addAttribute("listType", "disks rented by " + user.getFullName());
			}
			break;
			//This type of disks specifies render disks returned to owner by user
			case "returnedBy":
			{
				disks = diskService.getReturnedBy(userId);
				model.addAttribute("listType", "disks returned by " + user.getFullName());
			}
			break;
			//This type of disks specifies render user's free disks
			case "free":
			{
				disks = diskService.getUsersFree(userId);
				model.addAttribute("listType", user.getFullName()+"'s free disks");
			}
			break;
			//This type of disks specifies render free rent disks 
			case "forRent":
			{
				disks =  diskService.getFreeForRent(userId);
				model.addAttribute("listType", "all free for rent disks");
			}
			break;
			default:
				disks = null;
			break;
		}
		
		model.addAttribute("diskType", diskType);
		model.addAttribute("userId", userId);
		model.addAttribute("disks", disks);
		
		return "diskViews/disksList";
	}
	

    /**
     * Retrieves the add page
     *
     * @return the name of the JSP page
     */
	@RequestMapping(value="/user/{userId}/disk", method=RequestMethod.GET)
	public String getAddForm(Model model, @PathVariable("userId") int userId)
	{
		model.addAttribute("userId", userId);
		model.addAttribute("isForAdd", true);
		
		return "diskViews/addForm";
	}
	
    /**
     * Adds a new disk by delegating the processing to DiskService.
     * Displays a confirmation JSP page
     *
     * @return  the name of the JSP page
     */
	@RequestMapping(value="/user/{userId}/disk", method=RequestMethod.POST)
	public String addDisk(Model model, @ModelAttribute("disk") Disk disk, @PathVariable("userId") int userId)
	{
		if(disk.getDescription().isEmpty() || disk.getName().isEmpty())
		{
			model.addAttribute("error", "Each text field must be filled");
			model.addAttribute("isForAdd", true);
			
			return "diskViews/addForm";
		}
		
		disk.setUser(userService.getById(userId));
		diskService.add(disk);
		
		return "redirect:/user/"+String.valueOf(userId)+"/disks?type=free";
	}

    /**
     * Retrieves the edit page
     *
     * @return the name of the JSP page
     */
	@RequestMapping(value="/user/{userId}/disk/{diskId}", method=RequestMethod.GET)
	public String getEditForm(Model model, @PathVariable("userId") int userId, @PathVariable("diskId") int diskId)
	{
		Disk disk = diskService.getById(diskId);
		model.addAttribute("disk",disk);
		model.addAttribute("userId", userId);
		
		return "diskViews/addForm";
	}

    /**
     * Update or delete an existing disk by delegating the processing to DiskService.
     * Displays a confirmation JSP page
     *
     * @return  the name of the JSP page
     */
	@RequestMapping(value="/user/{userId}/disk/{diskId}", method=RequestMethod.POST)
	public String editDisk(Model model, @ModelAttribute("disk") Disk disk, @PathVariable("userId") int userId,
			@PathVariable("diskId") int diskId, @RequestParam("action") String action)
	{
		switch(action)
		{
			//This type of action specifies edit existing disk
			case "edit":
			{
				disk.setUser(userService.getById(userId));
				disk.setId(diskId);
				diskService.edit(disk);
				
				return "redirect:/user/"+String.valueOf(userId)+"/disks?type=free";
			}
			//This type of action specifies return to owner rented by user disk
			case "return":
			{
				TakenItem takenItem = takenItemService.get(userService.getById(userId),
						diskService.getById(diskId), true);
				takenItem.setRentedState(false);
				takenItemService.edit(takenItem);
				
				return "redirect:/user/"+String.valueOf(userId)+"/disks?type=rentedBy";
			}
			//This type of action specifies retrieve to user rented from user disk
			case "retrieve":
			{
				TakenItem searchingItem = null;
				List<TakenItem> takenItems = takenItemService.getByRentedState(true);
				for(TakenItem takenItem: takenItems)
					if(takenItem.getDisk().getId() == diskId && takenItem.getDisk().getUser().getId() == userId)
					{
						searchingItem = takenItem;
						searchingItem.setRentedState(false);
						takenItemService.edit(searchingItem);
						break;
					}
				
				return "redirect:/user/"+String.valueOf(userId)+"/disks?type=rentedFrom";
			}
			//This type of action specifies user rent an existing disk
			case "rent":
			{
				TakenItem takingItem = new TakenItem();
				takingItem.setDisk(diskService.getById(diskId));
				takingItem.setUser(userService.getById(userId));
				takingItem.setStartDate( new java.sql.Date(Calendar.getInstance().getTimeInMillis()) );
				takingItem.setRentedState(true);

				takenItemService.add(takingItem);
				
				return "redirect:/user/"+String.valueOf(userId)+"/disks?type=rentedBy";
			}
			//This type of action specifies delete an existing disk
			case "delete":
				diskService.delete(diskId);
			break;
		}
		
		return "redirect:/user/"+String.valueOf(userId)+"/disks?type=free";
	}
}
