package com.task.tech.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.task.tech.entities.Disk;
import com.task.tech.entities.User;

/**
 * Data access object for processing Disks.
 *
 */
@Repository
public class DiskDAO 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Service for processing Disks.
	 *
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getAll()
	{
		Session session = sessionFactory.getCurrentSession();
		   
		Query query = session.createQuery("FROM  Disk");
		   
		return  query.list();
	}
	
	/**
	 * Retrieves all free for rent disks of specified user. 
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getUsersFree(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.user.id = :user_id AND d.id NOT IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = true)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves all disks rented from specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getRentedFrom(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.user.id = :user_id AND d.id IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = true)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves all disks returned to specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getReturnedTo(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.user.id = :user_id AND d.id IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = false)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves all disks rented from owner by specified user. 
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getRentedBy(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.id IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = true and ti.pk.user.id = :user_id)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves all disks returned to owner by specified rent.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getReturnedBy(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.id IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = false and ti.pk.user.id = :user_id)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves all free for rent disks for specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getFreeForRent(int userId)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "SELECT d FROM Disk d "
				+ "WHERE d.user.id <> :user_id AND d.id NOT IN "
				+ "(SELECT ti.pk.disk.id FROM TakenItem ti WHERE ti.rentedState = true)";
		Query query = session.createQuery(hql);

		query.setParameter("user_id", userId);
		
		return query.list();
	}
	
	/**
	 * Retrieves specified disk.
	 * @param id is the id of the existing disk
	 * @return a disk
	 */
	public Disk getById(Integer id) 
	{
		Session session = sessionFactory.getCurrentSession();
	   
		Disk disk = (Disk)session.get(Disk.class, id);
	   
		return disk;
	}
	 
	/**
	 * Retrieves specified disk.
	 * @param user is the existing user
	 * @return a list of disks
	 */
	@SuppressWarnings("unchecked")
	public List<Disk> getByUser(User user) 
	{
		Session session = sessionFactory.getCurrentSession();
	   
		Criteria crirteria = session.createCriteria(Disk.class);
		
		crirteria.add(Restrictions.eq("user", user));
		  
		List<Disk> disks = crirteria.list(); 
		
		return disks;
	}
	
	/**
	 * Add to database specified disk.
	 * @param disk is the disk for adding
	 * 
	 */
	public void add(Disk Disk) 
	{
		Session session = sessionFactory.getCurrentSession();
	   
		session.save(Disk);
	}
	 
	/**
	 * Delete from database specified disk.
	 * @param disk is the disk for deleting
	 * 
	 */
	public void delete(Integer id)
	{
		Session session = sessionFactory.getCurrentSession();
   
		Disk Disk = (Disk)session.get(Disk.class, id);
   
		session.delete(Disk);
	}
	
	/**
	 * Update existing disk.
	 * @param disk is the disk for update
	 * 
	 */
	public void edit(Disk disk)
	{
		  Session session = sessionFactory.getCurrentSession();
		   
		  Disk existingDisk = (Disk)session.get(Disk.class, disk.getId());
		   
		  existingDisk.setName(disk.getName());
		  existingDisk.setDescription(disk.getDescription());
		 
		  session.save(existingDisk);
	}
}
