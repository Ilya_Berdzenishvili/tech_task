-- -----------------------------------------------------
-- Schema techtask
-- -----------------------------------------------------
drop table IF EXISTS `techtask`.taken_items;
drop table IF EXISTS `techtask`.disks;
drop table IF EXISTS `techtask`.users;
drop SCHEMA IF EXISTS `techtask`;

CREATE SCHEMA IF NOT EXISTS `techtask` DEFAULT CHARACTER SET utf8 ;
USE `techtask` ;
-- -----------------------------------------------------
-- Table `techtask`.`USERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `techtask`.`USERS` (
  `USER_ID` INT NOT NULL AUTO_INCREMENT,
  `FULL_NAME` VARCHAR(45) NOT NULL,
  `LOGIN` VARCHAR(45) NOT NULL,
  `PASSWORD` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`USER_ID`));
                         
-- -----------------------------------------------------
-- Table `techtask`.`DISKS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `techtask`.`DISKS` (
  `DISK_ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(45) NOT NULL,
  `DESCRIPTION` VARCHAR(45) NOT NULL,
  `USER_ID` INT NOT NULL,
  PRIMARY KEY (`DISK_ID`),
  INDEX `fk_DISKS_USERS1_idx` (`USER_ID` ASC),
  CONSTRAINT `fk_DISKS_USERS1`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `techtask`.`USERS` (`USER_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
                        
-- -----------------------------------------------------
-- Table `techtask`.`TAKEN_ITEMS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `techtask`.`TAKEN_ITEMS` (
  `USER_ID` INT NOT NULL,
  `DISK_ID` INT NOT NULL,
  `START_DATE` DATETIME NOT NULL,
  `RENTED_STATE` TINYINT(1) NOT NULL,
  PRIMARY KEY (`USER_ID`, `DISK_ID`, `START_DATE`),
  INDEX `fk_USERS_has_DISKS_DISKS2_idx` (`DISK_ID` ASC),
  INDEX `fk_USERS_has_DISKS_USERS2_idx` (`USER_ID` ASC),
  CONSTRAINT `fk_USERS_has_DISKS_USERS2`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `techtask`.`USERS` (`USER_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_USERS_has_DISKS_DISKS2`
    FOREIGN KEY (`DISK_ID`)
    REFERENCES `techtask`.`DISKS` (`DISK_ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);