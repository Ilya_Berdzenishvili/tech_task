package com.task.tech.dao;

import java.sql.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.task.tech.entities.Disk;
import com.task.tech.entities.TakenItem;
import com.task.tech.entities.TakenItemPK;
import com.task.tech.entities.User;

/**
 * Data access object for processing TakenItems.
 *
 */
@Repository
public class TakenItemDAO 
{
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Retrieves all taken items.
	 *
	 * @return a list of taken items
	 */
	@SuppressWarnings("unchecked")
	public List<TakenItem> getAll()
	{
		Session session = sessionFactory.getCurrentSession();
		   
		Query query = session.createQuery("FROM  TakenItem");
		   
		return  query.list();
	}

	/**
	 * Retrieves taken items specified by user-arendator.
	 * @param arendator is the existing user
	 * @return a list of taken items
	 */
	@SuppressWarnings("unchecked")
	public List<TakenItem> getByArendator(User arendator) 
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr =  session.createCriteria(TakenItem.class);
		cr.add(Restrictions.eq("pk.user", arendator));
		
		return cr.list();
	}
	
	/**
	 * Retrieves taken items specified by rented disk.
	 * @param disk is the existing disk
	 * @return a list of taken items
	 */
	@SuppressWarnings("unchecked")
	public List<TakenItem> getByDisk(Disk disk) 
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr =  session.createCriteria(TakenItem.class);
		cr.add(Restrictions.eq("pk.disk", disk));
		
		return cr.list();
	}
	
	/**
	 * Retrieves taken items specified by start date of rent.
	 * @param startDate is the start date of rent
	 * @return a list of taken items
	 */
	@SuppressWarnings("unchecked")
	public List<TakenItem> getByStartDate(Date startDate) 
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr =  session.createCriteria(TakenItem.class);
		cr.add(Restrictions.eq("pk.startDate", startDate));
		
		return cr.list();
	}
	
	/**
	 * Retrieves taken items specified by rented state.
	 * @param isRented specified rented state
	 * @return a list of taken items
	 */
	@SuppressWarnings("unchecked")
	public List<TakenItem> getByRentedState(Boolean rentedState) 
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr =  session.createCriteria(TakenItem.class);
		cr.add(Restrictions.eq("rentedState", rentedState));
		
		return cr.list();
	}
	
	/**
	 * Retrieves taken item specified by user, date and start date of the rent.
	 * @param user is the existing user
	 * @param disk is the existing disk
	 * @param startDate is the start date of rent
	 * @return a taken item
	 */
	public TakenItem get(User user, Disk disk, Date startDate)
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(TakenItem.class);
		
		Criterion crUser =  Restrictions.eq("pk.user", user);
		Criterion crDisk =  Restrictions.eq("pk.disk", disk);
		Criterion crDate =  Restrictions.eq("pk.startDate", startDate);
		
		Conjunction expr = Restrictions.and(crUser, crDisk, crDate);
		cr.add(expr);
		
		return (TakenItem)cr.uniqueResult();		
	}
	
	/**
	 * Retrieves taken item specified by user, date and state of the rent.
	 * @param user is the existing user
	 * @param disk is the existing disk
	 * @param rentedState specified rented state
	 * @return a taken item
	 */
	public TakenItem get(User user, Disk disk, Boolean rentedState)
	{
		Session session =  sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(TakenItem.class);
		
		Criterion crUser =  Restrictions.eq("pk.user", user);
		Criterion crDisk =  Restrictions.eq("pk.disk", disk);
		Criterion crDate =  Restrictions.eq("rentedState", rentedState);
		
		Conjunction expr = Restrictions.and(crUser, crDisk, crDate);
		cr.add(expr);
		
		return (TakenItem)cr.uniqueResult();		
	}
	
	/**
	 * Add to database specified taken item.
	 * @param takenItem is the take item for adding
 	 * 
	 */
	public void add(TakenItem takenItem) 
	{
		Session session = sessionFactory.getCurrentSession();
	   
		session.save(takenItem);
	}
	 
	/**
	 * Delete from database specified taken item.
	 * @param takenItemPK is the taken item's composite key
	 * 
	 */
	public void delete(TakenItemPK takenItemPK)
	{
		Session session = sessionFactory.getCurrentSession();
   
		TakenItem takenItem = (TakenItem)session.get(TakenItem.class, takenItemPK);
   
		session.delete(takenItem);
	}
	
	/**
	 * Update existing taken item.
	 * @param takenItem is the taken item for update
	 * 
	 */
	public void edit(TakenItem takenItem)
	{
		Session session = sessionFactory.getCurrentSession();
		String hql = "UPDATE TakenItem set rentedState = :new_rented_state "
				+ "WHERE pk.user.id = :user_id and pk.disk.id = :disk_id and pk.startDate = :start_Date";
		Query query = session.createQuery(hql);
		query.setParameter("new_rented_state", takenItem.getRentedState());
		query.setParameter("start_Date", takenItem.getStartDate());
		query.setParameter("user_id", takenItem.getUser().getId());
		query.setParameter("disk_id", takenItem.getDisk().getId());
		query.executeUpdate();
	}
}
