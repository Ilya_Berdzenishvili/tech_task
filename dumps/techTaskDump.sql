CREATE DATABASE  IF NOT EXISTS `techtask` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `techtask`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: techtask
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `disks`
--

DROP TABLE IF EXISTS `disks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disks` (
  `DISK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`DISK_ID`),
  KEY `fk_DISKS_USERS1_idx` (`USER_ID`),
  CONSTRAINT `fk_DISKS_USERS1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disks`
--

LOCK TABLES `disks` WRITE;
/*!40000 ALTER TABLE `disks` DISABLE KEYS */;
INSERT INTO `disks` VALUES (1,'Classic music','Your classic songs',1),(2,'Classic movies','Your classic movies',1),(3,'Classic games','Your classic games',1),(4,'Exhibitions','Your scientific exhibitions',1),(5,'Videos','Your videos',2),(6,'Pictures','Your pictures',2),(7,'Documents','Your documents',2),(8,'Projects','Your projects',3),(9,'Presentations','Your presentations',3),(10,'Messages','Your messages',4),(11,'Phone numbers','Freinds\' phone numbers',4);
/*!40000 ALTER TABLE `disks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taken_items`
--

DROP TABLE IF EXISTS `taken_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taken_items` (
  `USER_ID` int(11) NOT NULL,
  `DISK_ID` int(11) NOT NULL,
  `START_DATE` datetime NOT NULL,
  `RENTED_STATE` tinyint(1) NOT NULL,
  PRIMARY KEY (`USER_ID`,`DISK_ID`,`START_DATE`),
  KEY `fk_USERS_has_DISKS_DISKS2_idx` (`DISK_ID`),
  KEY `fk_USERS_has_DISKS_USERS2_idx` (`USER_ID`),
  CONSTRAINT `fk_USERS_has_DISKS_DISKS2` FOREIGN KEY (`DISK_ID`) REFERENCES `disks` (`DISK_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_USERS_has_DISKS_USERS2` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taken_items`
--

LOCK TABLES `taken_items` WRITE;
/*!40000 ALTER TABLE `taken_items` DISABLE KEYS */;
INSERT INTO `taken_items` VALUES (1,5,'2016-03-20 09:13:33',1),(1,6,'2016-03-19 09:10:12',1),(2,1,'2016-03-16 15:11:30',1),(2,2,'2016-03-19 11:12:48',1);
/*!40000 ALTER TABLE `taken_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FULL_NAME` varchar(45) NOT NULL,
  `LOGIN` varchar(45) NOT NULL,
  `PASSWORD` varchar(45) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sergey','Sergo','111'),(2,'Ivan','Vano','222'),(3,'Petr','Petro','333'),(4,'Nikolay','Niko','444'),(5,'Michael','Mishiko','555');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-22 10:16:36
