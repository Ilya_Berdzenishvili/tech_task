<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Disk's page</title>
<style type="text/css">
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}

tr:hover {
	background-color: #f5f5f5
}
</style>
</head>
<body>
	<h2>List of ${listType}</h2>
	<hr align="left" size="1" color="#ff0000" />
	<table border="1">
		<tr>
			<td>Nr:</td>
			<td><b>Name</b></td>
			<td><b>Description</b></td>
			<c:choose>
				<c:when test="${diskType == 'free'}">
					<td><b>Edit</b></td>
					<td><b>Delete</b></td>
				</c:when>
				<c:when test="${diskType == 'rentedFrom'}">
					<td><b>Regain</b></td>
				</c:when>
				<c:when test="${diskType == 'rentedBy'}">
					<td><b>Return</b></td>
				</c:when>
				<c:when test="${diskType == 'forRent'}">
					<td><b>Rent</b></td>
				</c:when>
			</c:choose>
		</tr>
		<c:forEach var="disk" items="${disks}" varStatus="status">
			<tr>
				<td><c:out value="${status.count}" /></td>
				<td><c:out value="${disk.name}" /></td>
				<td><c:out value="${disk.description}" /></td>
				<c:choose>
					<c:when test="${diskType == 'free'}">
						<td><form action="/user/${userId}/disk/${disk.id}"
								method="GET">
								<input type="image"
									src="<c:url value="/resources/images/editIcon.jpg"/>" />
							</form></td>
						<td><form
								action="/user/${userId}/disk/${disk.id}?action=delete"
								method="POST">
								<input type="image"
									src="<c:url value="/resources/images/delIcon.jpg"/>" />
							</form></td>
					</c:when>
					<c:when test="${diskType == 'rentedFrom'}">
						<td><form
								action="/user/${userId}/disk/${disk.id}?action=retrieve"
								method="POST">
								<input type="image"
									src="<c:url value="/resources/images/leftArrow.jpg"/>" />
							</form></td>
					</c:when>
					<c:when test="${diskType == 'rentedBy'}">
						<td><form
								action="/user/${userId}/disk/${disk.id}?action=return"
								method="POST">
								<input type="image"
									src="<c:url value="/resources/images/rightArrow.jpg"/>" />
							</form></td>
					</c:when>
					<c:when test="${diskType == 'forRent'}">
						<td><form
								action="/user/${userId}/disk/${disk.id}?action=rent"
								method="POST">
								<input type="image"
									src="<c:url value="/resources/images/plus.jpg"/>" />
							</form></td>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</table>
	<p>
		<a href="/user/${userId}"> Move to profile page</a>
	</p>
	<hr align="left" size="1" color="#ff0000" />
	<p>
		<a href="/login"> Log out</a>
	</p>
</body>
</html>