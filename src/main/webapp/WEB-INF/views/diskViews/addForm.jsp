<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add page</title>
<link type="text/css"
	href='<spring:url value="/resources/css/style.css"/>' rel="stylesheet" />
<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#adding-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>
</head>
<body>
	<c:if test="${isForAdd}">
		<c:set var="actionVariable" value="/user/${userId}/disk/${diskId}" />
		<c:set var="btnName" value="Add disk" />
		<c:set var="formName" value="Add disk page" />
	</c:if>
	<c:if test="${!isForAdd}">
		<c:set var="btnName" value="Edit" />
		<c:set var="formName" value="Edit disk page" />
	</c:if>
	<h2 align="center">${formName}</h2>
	<hr align="left" size="1" color="#ff0000" />
	<c:set var="actionVariable"
		value="/user/${userId}/disk/${diskId}?action=edit" />
	<div id="adding-box">
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<div class="msg">Please, fill each text field</div>
		<s:form action="${actionVariable}" method="post" modelAttribute="disk">
			<table>
				<tr>
					<td>Name:</td>
					<td><input type="text" name="name" value="${disk.name}"></td>
				</tr>
				<tr>
					<td>Description:</td>
					<td><input type="text" name="description"
						value="${disk.description}"></td>
				</tr>
			</table>
			<p align="center">
				<input type="submit" value="${btnName}">
		</s:form>
	</div>
	<p align="center">
		<a href="/user/${userId}"> Move to profile page</a>
	<hr align="left" size="1" color="#ff0000" />
	<p align="center">
		<a href="/login"> Log out</a>
</body>
</html>