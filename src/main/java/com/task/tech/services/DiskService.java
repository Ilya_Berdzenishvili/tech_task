package com.task.tech.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.tech.dao.DiskDAO;
import com.task.tech.entities.Disk;
import com.task.tech.entities.User;

/**
 * Service for processing Disks.
 *
 */
@Service
@Transactional
public class DiskService 
{
	@Autowired
	private DiskDAO diskDAO;
	
	/**
	 * Retrieves all disks.
	 *
	 * @return a list of disks
	 */
	public List<Disk> getAll()
	{		   
		return  diskDAO.getAll();
	}
	
	/**
	 * Retrieves all free for rent disks of specified user. 
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getUsersFree(int userId)
	{
		return diskDAO.getUsersFree(userId);
	}
	
	/**
	 * Retrieves all disks rented from specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getRentedFrom(int userId)
	{
		return diskDAO.getRentedFrom(userId);
	}
	
	/**
	 * Retrieves all disks returned to specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getReturnedTo(int userId)
	{
		return diskDAO.getReturnedTo(userId);
	}
	
	/**
	 * Retrieves all disks rented from owner by specified user. 
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getRentedBy(int userId)
	{
		return diskDAO.getRentedBy(userId);
	}
	
	/**
	 * Retrieves all disks returned to owner by specified rent.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getReturnedBy(int userId)
	{
		return diskDAO.getReturnedBy(userId);
	}
	
	/**
	 * Retrieves all free for rent disks for specified user.
	 * @param userId is the id of the existing user
	 * @return a list of disks
	 */
	public List<Disk> getFreeForRent(int userId)
	{
		return diskDAO.getFreeForRent(userId);
	}
	
	/**
	 * Retrieves specified disk.
	 * @param id is the id of the existing disk
	 * @return a disk
	 */
	public Disk getById(Integer id) 
	{
		return diskDAO.getById(id);
	}
	 
	/**
	 * Retrieves specified disk.
	 * @param user is the existing user
	 * @return a list of disks
	 */
	public List<Disk> getByUser(User user) 
	{
		return diskDAO.getByUser(user);
	}
	
	/**
	 * Add to database specified disk.
	 * @param disk is the disk for adding
	 * 
	 */
	public void add(Disk disk) 
	{
		diskDAO.add(disk);
	}

	/**
	 * Delete from database specified disk.
	 * @param disk is the disk for deleting
	 * 
	 */
	public void delete(Integer id)
	{
		diskDAO.delete(id);
	}
	
	/**
	 * Update existing disk.
	 * @param disk is the disk for update
	 * 
	 */
	public void edit(Disk disk)
	{
		diskDAO.edit(disk);
	}
}
