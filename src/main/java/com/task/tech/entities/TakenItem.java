package com.task.tech.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TAKEN_ITEMS")
@AssociationOverrides({
	@AssociationOverride(name = "pk.user", 
		joinColumns = @JoinColumn(name = "USER_ID")),
	@AssociationOverride(name = "pk.disk", 
		joinColumns = @JoinColumn(name = "DISK_ID")) })
public class TakenItem implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5857613215235764215L;

	@EmbeddedId
	private TakenItemPK pk = new TakenItemPK();
	
	@Column(name="RENTED_STATE")
	private Boolean rentedState;
	
	@Transient
	public User getUser()
	{
		return getPk().getUser();
	}
	
	public void setUser(User user)
	{
		getPk().setUser(user);
	}
	
	@Transient
	public Disk getDisk()
	{
		return getPk().getDisk();
	}
	
	public void setDisk(Disk disk)
	{
		getPk().setDisk(disk);
	}

	@Transient
	public Date getStartDate()
	{
		return getPk().getStartDate();
	}
	
	public void setStartDate(Date startDate)
	{
		getPk().setStartDate(startDate);
	}
	
	public TakenItemPK getPk() {
		return pk;
	}

	public void setPk(TakenItemPK pk) {
		this.pk = pk;
	}

	public Boolean getRentedState() {
		return rentedState;
	}

	public void setRentedState(Boolean rentedState) {
		this.rentedState = rentedState;
	}
	
}
