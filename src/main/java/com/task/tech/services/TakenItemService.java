package com.task.tech.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.tech.dao.TakenItemDAO;
import com.task.tech.entities.Disk;
import com.task.tech.entities.TakenItem;
import com.task.tech.entities.TakenItemPK;
import com.task.tech.entities.User;

/**
 * Service for processing TakenItems.
 *
 */
@Service
@Transactional
public class TakenItemService
{
	@Autowired
	TakenItemDAO takenItemDAO;
	
	/**
	 * Retrieves all taken items.
	 *
	 * @return a list of taken items
	 */
	public List<TakenItem> getAll()
	{
		return takenItemDAO.getAll();
	}
	
	/**
	 * Retrieves taken items specified by user-arendator.
	 * @param arendator is the existing user
	 * @return a list of taken items
	 */
	public List<TakenItem> getByArendator(User arendator)
	{
		return takenItemDAO.getByArendator(arendator);
	}

	/**
	 * Retrieves taken items specified by rented disk.
	 * @param disk is the existing disk
	 * @return a list of taken items
	 */
	public List<TakenItem> getByDisk(Disk disk)
	{
		return takenItemDAO.getByDisk(disk);
	}

	/**
	 * Retrieves taken items specified by start date of rent.
	 * @param startDate is the start date of rent
	 * @return a list of taken items
	 */
	public List<TakenItem> getByStartDate(Date startDate)
	{
		return takenItemDAO.getByStartDate(startDate);
	}

	/**
	 * Retrieves taken items specified by rented state.
	 * @param isRented specified rented state
	 * @return a list of taken items
	 */
	public List<TakenItem> getByRentedState(Boolean rentedState) 
	{
		return takenItemDAO.getByRentedState(rentedState);
	}
	
	/**
	 * Retrieves taken item specified by user, date and start date of the rent.
	 * @param user is the existing user
	 * @param disk is the existing disk
	 * @param startDate is the start date of rent
	 * @return a taken item
	 */
	public TakenItem get(User user, Disk disk, Date startDate)
	{
		return takenItemDAO.get(user, disk, startDate);
	}
	
	/**
	 * Retrieves taken item specified by user, date and state of the rent.
	 * @param user is the existing user
	 * @param disk is the existing disk
	 * @param rentedState specified rented state
	 * @return a taken item
	 */
	public TakenItem get(User user, Disk disk, Boolean rentedState)
	{
		return takenItemDAO.get(user, disk, rentedState);
	}

	/**
	 * Add to database specified taken item.
	 * @param takenItem is the take item for adding
 	 * 
	 */
	public void add(TakenItem takenItem)
	{
		takenItemDAO.add(takenItem);
	}
	
	/**
	 * Delete from database specified taken item.
	 * @param takenItemPK is the taken item's composite key
	 * 
	 */
	public void delete(TakenItemPK takenItemPK)
	{
		takenItemDAO.delete(takenItemPK);
	}
	
	/**
	 * Update existing taken item.
	 * @param takenItem is the taken item for update
	 * 
	 */
	public void edit(TakenItem takenItem)
	{
		takenItemDAO.edit(takenItem);
	}
}
