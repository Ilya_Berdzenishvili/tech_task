<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${user.fullName}'spersonalpage</title>
</head>
<body>
	<h2>${user.fullName}'spersonal page</h2>
	<p>
		<b>User's login: </b>
		<c:out value="${user.login}" />
	<p>
		<b>User's full name: </b>
		<c:out value="${user.fullName}" />
	<hr align="left" size="1" color="#ff0000" />
	<p>
		<a href="/user/${user.id}/disks?type=free"> Get free disks</a>
	<p>
		<a href="/user/${user.id}/disks?type=rentedBy"> Get disks rented
			by ${user.fullName}</a>
	<p>
		<a href="/user/${user.id}/disks?type=returnedBy"> Get disks
			returned by ${user.fullName}</a>
	<p>
		<a href="/user/${user.id}/disks?type=rentedFrom"> Get disks rented
			from ${user.fullName}</a>
	<p>
		<a href="/user/${user.id}/disks?type=returnedTo"> Get disks
			returned to ${user.fullName}</a>
	<p>
		<a href="/user/${user.id}/disks?type=forRent"> Get disk for rent</a>
	<p>
		<a href="/user/${user.id}/disk"> Create new disk</a>
	<hr align="left" size="1" color="#ff0000" />
	<p>
		<a href="/login"> Log out</a>
	</p>
</body>
</html>