package com.task.tech.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.task.tech.entities.User;
import com.task.tech.services.UserService;

/**
 * Handles and retrieves request for authorization
 */
@Controller
public class MainController 
{
	@Autowired
	private UserService userService;

    /**
     * Retrieves the login page
     *
     * @return the name of the JSP page
     */
	@RequestMapping(value="*", method=RequestMethod.GET)
	public String getProfile()
	{
		return "userViews/login";
	}
	
    /**
     * Retrieves the user's authorization
     *
     * @return the name of the JSP page
     */
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String getProfile(Model model, @RequestParam("login") String login, @RequestParam("password") String password)
	{
		User user = userService.getByLogin(login, password);
		if(user == null)
		{
			model.addAttribute("error", "Invalid username and password");
			model.addAttribute("msg", "Plase, enter correct data");
			
			return "userViews/login";
		}
		
		model.addAttribute("user",user);
		
		return "userViews/profile";
	}
	
    /**
     * Retrieves the user's personal page
     *
     * @return the name of the JSP page
     */	
	@RequestMapping(value="/user/{userId}", method=RequestMethod.GET)
	public String getProfile(Model model, @PathVariable("userId") int userId)
	{
		User user = userService.getById(userId);
		model.addAttribute("user",user);
		
		return "userViews/profile";
	}
}
