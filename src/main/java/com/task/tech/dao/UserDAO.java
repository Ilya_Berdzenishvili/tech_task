package com.task.tech.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.task.tech.entities.User;

/**
 * Data access object for processing Users.
 *
 */
@Repository
public class UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Retrieves all users.
	 *
	 * @return a list of users
	 */
	@SuppressWarnings("unchecked")
	public List<User> getAll() {
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM  User");

		return query.list();
	}

	/**
	 * Retrieves specified user.
	 * 
	 * @param id
	 *            is the id of the existing user
	 * @return a user
	 */
	public User get(Integer id) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.get(User.class, id);

		return user;
	}

	/**
	 * Retrieves specified by login and password user.
	 * 
	 * @param login
	 *            is the login of the existing user
	 * @param password
	 *            is the password of the existing user
	 * @return a user
	 */
	public User getByLogin(String login, String password) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(User.class);

		Criterion crLogin = Restrictions.eq("login", login);
		Criterion crPassword = Restrictions.eq("password", password);

		LogicalExpression expr = Restrictions.and(crLogin, crPassword);
		cr.add(expr);
		User user = (User) cr.uniqueResult();

		return user;
	}

	/**
	 * Add to database specified user.
	 * 
	 * @param user
	 *            is the user for adding
	 * 
	 */
	public void add(User user) {
		Session session = sessionFactory.getCurrentSession();

		session.save(user);
	}

	/**
	 * Delete from database specified user.
	 * 
	 * @param id
	 *            is the user's id for deleting
	 * 
	 */
	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.get(User.class, id);

		session.delete(user);
	}

	/**
	 * Update existing user.
	 * 
	 * @param user
	 *            is the user for update
	 * 
	 */
	public void edit(User user) {
		Session session = sessionFactory.getCurrentSession();

		User existingUser = (User) session.get(User.class, user.getId());

		existingUser.setFullName(user.getFullName());
		existingUser.setLogin(user.getLogin());
		existingUser.setPassword(user.getPassword());

		session.save(existingUser);
	}
}
