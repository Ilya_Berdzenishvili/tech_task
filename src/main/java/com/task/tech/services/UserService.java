package com.task.tech.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.task.tech.dao.UserDAO;
import com.task.tech.entities.User;

/**
 * Service for processing Users.
 *
 */
@Service
@Transactional
public class UserService {
	@Autowired
	private UserDAO userDAO;

	/**
	 * Retrieves all users.
	 *
	 * @return a list of users
	 */
	public List<User> getAll() {
		return userDAO.getAll();
	}

	/**
	 * Retrieves specified user.
	 * 
	 * @param id
	 *            is the id of the existing user
	 * @return a user
	 */
	public User getById(Integer id) {
		return userDAO.get(id);
	}

	/**
	 * Retrieves specified by login and password user.
	 * 
	 * @param login
	 *            is the login of the existing user
	 * @param password
	 *            is the password of the existing user
	 * @return a user
	 */
	public User getByLogin(String login, String password) {
		return userDAO.getByLogin(login, password);
	}

	/**
	 * Add to database specified user.
	 * 
	 * @param user
	 *            is the user for adding
	 * 
	 */
	public void add(User user) {
		userDAO.add(user);
	}

	/**
	 * Delete from database specified user.
	 * 
	 * @param id
	 *            is the user's id for deleting
	 * 
	 */
	public void delete(Integer id) {
		userDAO.delete(id);
	}

	/**
	 * Update existing user.
	 * 
	 * @param user
	 *            is the user for update
	 * 
	 */
	public void edit(User user) {
		userDAO.edit(user);
	}
}
